﻿using System.Collections.Generic;

namespace UnityLog
{
    internal class TypeInfo
    {
        public bool AllMethods { get; set; }
        public bool AllIgnores { get; set; }
        public HashSet<string> Methods { get; set; } = new HashSet<string>();
        public HashSet<string> Ignores { get; set; } = new HashSet<string>();
    }
}
