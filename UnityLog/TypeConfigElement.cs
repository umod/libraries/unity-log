﻿using System.Configuration;

namespace UnityLog
{
    public class TypeConfigElement : ConfigurationElement
    {
        [ConfigurationProperty("name", DefaultValue = "*", IsRequired = true, IsKey = true)]
        public string Name
        {
            get { return (string) this["name"]; }
            set { this["name"] = value; }
        }

        [ConfigurationProperty("type", DefaultValue = "*", IsRequired = false)]
        public string Type
        {
            get { return (string) this["type"]; }
            set { this["type"] = value; }
        }

        [ConfigurationProperty("method", DefaultValue = "*", IsRequired = false)]
        public string Method
        {
            get { return (string) this["method"]; }
            set { this["method"] = value; }
        }

        [ConfigurationProperty("ignore", DefaultValue = "", IsRequired = false)]
        public string Ignore
        {
            get { return (string) this["ignore"]; }
            set { this["ignore"] = value; }
        }
    }
}
