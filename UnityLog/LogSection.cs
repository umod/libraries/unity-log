﻿using System.Configuration;

namespace UnityLog
{
    public class LogSection : ConfigurationSection
    {
        [ConfigurationProperty("Types", IsDefaultCollection = false)]
        [ConfigurationCollection(typeof (TypesCollection), AddItemName = "add", ClearItemsName = "clear", RemoveItemName = "remove")]
        public TypesCollection Types => (TypesCollection) base["Types"];
    }
}
