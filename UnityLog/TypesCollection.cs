﻿using System.Configuration;

namespace UnityLog
{
    public class TypesCollection : ConfigurationElementCollection
    {
        public override ConfigurationElementCollectionType CollectionType => ConfigurationElementCollectionType.AddRemoveClearMap;

        public TypeConfigElement this[int index]
        {
            get { return (TypeConfigElement) BaseGet(index); }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        public new TypeConfigElement this[string name] => (TypeConfigElement) BaseGet(name);

        protected override ConfigurationElement CreateNewElement()
        {
            return new TypeConfigElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((TypeConfigElement) element).Name;
        }

        public int IndexOf(TypeConfigElement type)
        {
            return BaseIndexOf(type);
        }

        public void Add(TypeConfigElement type)
        {
            BaseAdd(type);
        }

        protected override void BaseAdd(ConfigurationElement element)
        {
            BaseAdd(element, false);
        }

        public void Remove(TypeConfigElement type)
        {
            if (BaseIndexOf(type) >= 0)
                BaseRemove(type.Name);
        }

        public void RemoveAt(int index)
        {
            BaseRemoveAt(index);
        }

        public void Remove(string name)
        {
            BaseRemove(name);
        }

        public void Clear()
        {
            BaseClear();
        }
    }
}
