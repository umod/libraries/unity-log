﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;

using Mono.Cecil;
using Mono.Cecil.Cil;
using Mono.Cecil.Rocks;

namespace UnityLog
{
    internal class Program
    {
        private const string PostFix = "_original";

        private static readonly Dictionary<string, TypeInfo> TypeInfos = new Dictionary<string, TypeInfo>();

        private static bool ReadCustomSection()
        {
            try
            {
                var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var logSection = config.GetSection("Log") as LogSection;
                if (logSection == null) return false;
                for (var i = 0; i < logSection.Types.Count; i++)
                {
                    var type = logSection.Types[i];
                    if (string.IsNullOrWhiteSpace(type.Type)) continue;
                    var typeName = type.Type.Trim();
                    TypeInfo[] types;
                    if (typeName.Equals("*"))
                    {
                        TypeInfo allTypes;
                        if (!TypeInfos.TryGetValue(typeName, out allTypes))
                            TypeInfos[typeName] = allTypes = new TypeInfo();
                        types = new[] {allTypes};
                    }
                    else
                    {
                        var typeNames = typeName.Split(',');
                        types = new TypeInfo[typeNames.Length];
                        for (var i1 = 0; i1 < typeNames.Length; i1++)
                        {
                            typeName = typeNames[i1].Trim();
                            TypeInfo info;
                            if (!TypeInfos.TryGetValue(typeName, out info))
                                TypeInfos[typeName] = info = new TypeInfo();
                            types[i1] = info;
                        }
                    }

                    foreach (var typeInfo in types)
                    {
                        if (type.Method.Equals("*")) typeInfo.AllMethods = true;
                        else if (!string.IsNullOrWhiteSpace(type.Method))
                        {
                            var names = type.Method.Split(',');
                            foreach (var t in names)
                            {
                                var methodName = t.Trim();
                                if (!string.IsNullOrWhiteSpace(methodName))
                                    typeInfo.Methods.Add(methodName);
                            }
                        }
                        if (type.Ignore.Equals("*")) typeInfo.AllIgnores = true;
                        else if (!string.IsNullOrWhiteSpace(type.Ignore))
                        {
                            var names = type.Ignore.Split(',');
                            foreach (var t in names)
                            {
                                var methodName = t.Trim();
                                if (!string.IsNullOrWhiteSpace(methodName))
                                    typeInfo.Ignores.Add(methodName);
                            }
                        }
                    }
                }
            }
            catch (ConfigurationErrorsException err)
            {
                Console.WriteLine("ReadCustomSection(string): {0}", err);
                return false;
            }
            return true;
        }

        private static void Main(string[] args)
        {
            if (args.Length == 0 || string.IsNullOrWhiteSpace(args[0]) || !File.Exists(args[0]))
            {
                Console.WriteLine("Please pass a file.");
                Console.ReadKey();
                return;
            }

            if (!ReadCustomSection())
            {
                Console.WriteLine("Failed to load config.");
                Console.ReadKey();
                return;
            }

            var fileName = Path.GetFileNameWithoutExtension(args[0])?.Replace(PostFix, "");
            var extension = Path.GetExtension(args[0]);
            if (!File.Exists(fileName + PostFix + extension))
            {
                File.Copy(fileName + extension, fileName + PostFix + extension);
            }
            var assem = LoadAssembly(Path.GetFullPath(fileName + PostFix + extension));
            var unity = LoadAssembly(Path.GetFullPath("UnityEngine.dll"));
            var assemTypes = GetTypes(assem).OrderBy(t => t.Name);
            var logMethod = GetTypes(unity).FirstOrDefault(t => t.Name.Equals("Debug"))?.Methods.FirstOrDefault(m => m.Name.Equals("Log") && m.Parameters.Count == 1);
            //var logMethod = assem.MainModule.Import(typeof(Console).GetMethod("WriteLine", new[] { typeof(string) })).Resolve();
            //var getType = assem.MainModule.TypeSystem.Object.Resolve().Methods.FirstOrDefault(m => m.Name.Equals("GetType"));
            var stringConcat = assem.MainModule.TypeSystem.String.Resolve().Methods.FirstOrDefault(m => m.Name.Equals("Concat") && m.Parameters.Count == 1 && m.Parameters[0].ParameterType is ArrayType && ((ArrayType) m.Parameters[0].ParameterType).ElementType.Resolve() == assem.MainModule.TypeSystem.Object.Resolve());
            TypeInfo allTypes;
            TypeInfos.TryGetValue("*", out allTypes);
            foreach (var type in assemTypes)
                PatchType(type, allTypes, stringConcat, logMethod);
            assem.Write(fileName + extension);
            Console.ReadKey();
        }

        private static void PatchType(TypeDefinition type, TypeInfo allTypes, MethodDefinition stringConcat, MethodDefinition logMethod)
        {
            var patchedMethods = new List<MethodDefinition>();
            if (allTypes != null)
            {
                foreach (var method in type.Methods)
                {
                    if (patchedMethods.Contains(method) || method.Name.Contains("<")) continue;
                    if (allTypes.AllIgnores || allTypes.Ignores.Any(method.Name.Equals)) continue;
                    if (!allTypes.AllMethods && !allTypes.Methods.Any(method.Name.Equals)) continue;
                    PatchMethod(method, type, stringConcat, logMethod);
                    patchedMethods.Add(method);
                }
            }

            var keys = TypeInfos.Keys.Where(k => type.Name.StartsWith(k, StringComparison.OrdinalIgnoreCase));
            foreach (var key in keys)
            {
                var typeInfo = TypeInfos[key];
                foreach (var method in type.Methods)
                {
                    if (patchedMethods.Contains(method) || method.Name.Contains("<")) continue;
                    if (typeInfo.AllIgnores || typeInfo.Ignores.Any(method.Name.Equals)) continue;
                    if (!typeInfo.AllMethods && !typeInfo.Methods.Any(method.Name.Equals)) continue;
                    PatchMethod(method, type, stringConcat, logMethod);
                    patchedMethods.Add(method);
                }
            }
            foreach (var nestedType in type.NestedTypes)
                PatchType(nestedType, allTypes, stringConcat, logMethod);
        }

        private static IEnumerable<TypeDefinition> GetTypes(AssemblyDefinition assembly)
        {
            return assembly.Modules.SelectMany(module => module.Types);
        }

        private static AssemblyDefinition LoadAssembly(string path)
        {
            var resolver = new DefaultAssemblyResolver();
            resolver.AddSearchDirectory(Path.GetDirectoryName(path));
            return AssemblyDefinition.ReadAssembly(path, new ReaderParameters {AssemblyResolver = resolver});
        }

        private static void PatchMethod(MethodDefinition method, TypeDefinition type, MethodDefinition stringConcat, MethodDefinition logMethod)
        {
            if (method == null || !method.HasBody) return;
            Console.WriteLine(method.Module.Name + " - " + type.Name + " - " + method.Name);
            method.Body.SimplifyMacros();
            var proc = method.Body.GetILProcessor();
            var first = method.Body.Instructions[0];
            var cur = 0;
            proc.InsertBefore(first, Instruction.Create(OpCodes.Nop));
            proc.InsertBefore(first, Instruction.Create(OpCodes.Ldc_I4, (method.Parameters.Count*2) + 1));
            proc.InsertBefore(first, Instruction.Create(OpCodes.Newarr, method.Module.TypeSystem.Object));
            proc.InsertBefore(first, Instruction.Create(OpCodes.Dup));
            proc.InsertBefore(first, Instruction.Create(OpCodes.Ldc_I4, cur++));
            proc.InsertBefore(first, Instruction.Create(OpCodes.Ldstr, type.FullName + ": " + method.Name));
            proc.InsertBefore(first, Instruction.Create(OpCodes.Stelem_Ref));
            foreach (var parameter in method.Parameters)
            {
                proc.InsertBefore(first, Instruction.Create(OpCodes.Dup));
                proc.InsertBefore(first, Instruction.Create(OpCodes.Ldc_I4, cur++));
                proc.InsertBefore(first, Instruction.Create(OpCodes.Ldstr, " "));
                proc.InsertBefore(first, Instruction.Create(OpCodes.Stelem_Ref));

                proc.InsertBefore(first, Instruction.Create(OpCodes.Dup));
                proc.InsertBefore(first, Instruction.Create(OpCodes.Ldc_I4, cur++));
                proc.InsertBefore(first, Instruction.Create(OpCodes.Ldarg, parameter));
                if (parameter.ParameterType.IsValueType) proc.InsertBefore(first, Instruction.Create(OpCodes.Box, parameter.ParameterType));
                proc.InsertBefore(first, Instruction.Create(OpCodes.Stelem_Ref));
            }
            proc.InsertBefore(first, Instruction.Create(OpCodes.Call, method.Module.Import(stringConcat)));
            proc.InsertBefore(first, Instruction.Create(OpCodes.Call, method.Module.Import(logMethod)));
            method.Body.OptimizeMacros();
        }
    }
}
